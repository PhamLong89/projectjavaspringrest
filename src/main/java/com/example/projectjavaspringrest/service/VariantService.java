package com.example.projectjavaspringrest.service;

import com.example.projectjavaspringrest.model.entity.Variant;
import com.example.projectjavaspringrest.model.form.VariantForm;
import com.example.projectjavaspringrest.repository.ProductRepository;
import com.example.projectjavaspringrest.repository.VariantRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VariantService {

  private final VariantRepository variantRepository;
  private final ProductRepository productRepository;
  private final ProductService productService;

  public Variant findBySKU(String sKU) {
    return variantRepository.findBysKU(sKU);
  }

  public Variant save(Variant variant) {
    return variantRepository.save(variant);
  }

  public List<Variant> saveAll(List<Variant> variants) {
    List<Variant> result = new ArrayList<>();
    for (Variant variant : variants) {
      result.add(variantRepository.save(variant));
    }
    return result;
  }

  public int deleteBySKU(String sKU) {
    return variantRepository.deleteBysKU(sKU);
  }

  public VariantForm convertToVariantForm(Variant variant) {
    var variantForm = new VariantForm();
    variantForm.setProductId(String.valueOf(variant.getProduct().getProductId()));
    variantForm.setColor(variant.getColor());
    variantForm.setSize(variant.getSize());
    variantForm.setSKU(variant.getSKU());
    variantForm.setQuantity(String.valueOf(variant.getQuantity()));
    variantForm.setImage1(variant.getImage1());
    variantForm.setImage2(variant.getImage2());
    return variantForm;
  }

  public List<Variant> findByProductIdAndColorAndSize(int productId, String color, String size) {
    var product = productRepository.findByProductId(productId);
    if (product == null) {
      return Collections.emptyList();
    }
    return variantRepository.findByProductAndColorAndSize(product, color, size);
  }

  public List<Variant> findByProductIdAndColor(int productId, String color) {
    var product = productRepository.findByProductId(productId);
    if (product == null) {
      return Collections.emptyList();
    }
    return variantRepository.findByProductAndColor(product, color);
  }
}
