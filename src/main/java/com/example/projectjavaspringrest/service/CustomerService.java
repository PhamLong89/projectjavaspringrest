package com.example.projectjavaspringrest.service;

import com.example.projectjavaspringrest.model.entity.Customer;
import com.example.projectjavaspringrest.repository.CustomerRepository;
import com.example.projectjavaspringrest.security.customer.CustomerDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService implements UserDetailsService {

  private final CustomerRepository customerRepository;

  public Customer findByEmail(String email) {
    return customerRepository.findByEmail(email);
  }

  public Customer save(Customer customer) {
    return customerRepository.save(customer);
  }

  public Page<Customer> findAll(Pageable pageable) {
    return customerRepository.findAll(pageable);
  }

  public Customer finById(int id) {
    return customerRepository.findByCustomerId(id);
  }

  public Page<Customer> findByName(String customerName, Pageable pageable) {
    return customerRepository.findByName(customerName, pageable);
  }

  public void changeStatus(Customer customer) {
    customerRepository
        .changeStatus(String.valueOf(customer.getStatusActive()), customer.getCustomerId());
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    var customer = customerRepository.findByEmail(email);
    if (customer == null) {
      throw new UsernameNotFoundException("Email không tồn tại!");
    }
    return new CustomerDetails(customer);
  }
}
