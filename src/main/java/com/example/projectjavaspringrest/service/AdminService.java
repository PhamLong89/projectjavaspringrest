package com.example.projectjavaspringrest.service;

import com.example.projectjavaspringrest.model.entity.Admin;
import com.example.projectjavaspringrest.repository.AdminRepository;
import com.example.projectjavaspringrest.security.admin.AdminDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AdminService implements UserDetailsService {

  private final AdminRepository adminRepository;

  public Admin save(Admin admin) {
    return adminRepository.save(admin);
  }

  // findByEmail
  public Admin findByEmail(String email) {
    return adminRepository.findByEmail(email);
  }

  public Page<Admin> findAll(Pageable pageable) {
    return adminRepository.findAll(pageable);
  }

  public Admin findById(int adminId) {
    return adminRepository.findByAdminId(adminId);
  }

  public Page<Admin> findByName(String adminName, Pageable pageable) {
    return adminRepository.findByName(adminName, pageable);
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    var admin = adminRepository.findByEmail(email);
    if (admin == null) {
      throw new UsernameNotFoundException("Email không tồn tại!");
    }
    return new AdminDetails(admin);
  }
}
