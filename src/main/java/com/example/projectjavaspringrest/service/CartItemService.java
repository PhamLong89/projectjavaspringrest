package com.example.projectjavaspringrest.service;

import com.example.projectjavaspringrest.model.entity.Cart;
import com.example.projectjavaspringrest.model.entity.CartItem;
import com.example.projectjavaspringrest.model.form.CartItemForm;
import com.example.projectjavaspringrest.repository.CartItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CartItemService {

  private final CartItemRepository cartItemRepository;

  public CartItem save(CartItem item) {
    return cartItemRepository.save(item);
  }

  public CartItem findByCartAndId(Cart cart, int id) {
    return cartItemRepository.findByCartAndId(cart, id);
  }

  public int deleteByIdAndCart(int id, Cart cart) {
    return cartItemRepository.deleteByIdAndCart(id, cart);
  }

  public CartItemForm convertToCartItemForm(CartItem item) {
    var itemForm = new CartItemForm();
    itemForm.setId(String.valueOf(item.getId()));
    itemForm.setQuantity(String.valueOf(item.getQuantity()));
    itemForm.setSKU(item.getVariant().getSKU());
    return itemForm;
  }
}
