package com.example.projectjavaspringrest.security.customer;

import com.example.projectjavaspringrest.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@Order(1)
public class CustomerSecurityConfig extends WebSecurityConfigurerAdapter {

  private final CustomerService customerService;

  @Bean
  public JwtAuthenticationFilterCus jwtAuthenticationFilterCus() {
    return new JwtAuthenticationFilterCus();
  }

  @Bean(name = "customer")
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    // Get AuthenticationManager bean
    return super.authenticationManagerBean();
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public DaoAuthenticationProvider authenticationProvider2() {
    var authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(customerService);
    authProvider.setPasswordEncoder(passwordEncoder());
    return authProvider;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(authenticationProvider2());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable();// Ngăn chặn request từ một domain khác

    http.requestMatchers()
        .antMatchers("/*", "/user/**", "/product/**", "/products/**", "/cart/**")
        .and().authorizeRequests().antMatchers("/user/**").hasRole("USER")
        .anyRequest().permitAll()
        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    // Thêm một lớp Filter kiểm tra jwt
    http.addFilterBefore(jwtAuthenticationFilterCus(), UsernamePasswordAuthenticationFilter.class);
  }
}
