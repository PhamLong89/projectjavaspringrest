package com.example.projectjavaspringrest.security.admin;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JwtTokenAdmin {

  // Đoạn  này là bí mật, chỉ có phía server biết
  private static final String JWT_SECRET = "shop-Unfashion-Admin";

  //Thời gian có hiệu lực của chuỗi jwt
  private static final long JWT_EXPIRATION = 604800000L;
  private static final Logger logger = LoggerFactory.getLogger(JwtTokenAdmin.class);

  // Tạo ra jwt từ thông tin admin
  public String generateToken(AdminDetails adminDetails) {
    var now = new Date();
    var expiryDate = new Date(now.getTime() + JWT_EXPIRATION);
    // Tạo chuỗi json web token từ email của admin.
    return Jwts.builder()
        .setSubject(adminDetails.getUsername())
        .setIssuedAt(now)
        .setExpiration(expiryDate)
        .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
        .compact();
  }

  // Lấy thông tin user từ jwt
  public String getEmailAdminFromJWT(String token) {
    var claims = Jwts.parser()
        .setSigningKey(JWT_SECRET)
        .parseClaimsJws(token)
        .getBody();
    return claims.getSubject();
  }

  public boolean validateToken(String authToken) {
    try {
      Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
      return true;
    } catch (SignatureException ex) {
      logger.error("Invalid JWT signature: {}", ex.getMessage());
    } catch (MalformedJwtException ex) {
      logger.error("Invalid JWT token: {}", ex.getMessage());
    } catch (ExpiredJwtException ex) {
      logger.error("JWT token is expired: {}", ex.getMessage());
    } catch (UnsupportedJwtException ex) {
      logger.error("JWT token is unsupported: {}", ex.getMessage());
    } catch (IllegalArgumentException ex) {
      logger.error("JWT claims string is empty: {}", ex.getMessage());
    }
    return false;
  }

}
