package com.example.projectjavaspringrest.security.admin;

import com.example.projectjavaspringrest.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@Order(2)
public class AdminSecurityConfig extends WebSecurityConfigurerAdapter {

  private final AdminService adminService;

  @Bean
  public JwtAuthenticationFilterAdmin jwtAuthenticationFilterAdmin() {
    return new JwtAuthenticationFilterAdmin();
  }

  @Bean(name = "admin")
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    // Get AuthenticationManager bean
    return super.authenticationManagerBean();
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public DaoAuthenticationProvider authenticationProvider1() {
    var authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(adminService);
    authProvider.setPasswordEncoder(passwordEncoder());
    return authProvider;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(authenticationProvider1());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable();// Ngăn chặn request từ một domain khác

    http.authorizeRequests()
        .antMatchers("/manager/login").permitAll();

    http.requestMatchers ()
        .antMatchers("/manager/**")
        .and().authorizeRequests().antMatchers("/manager", "/manager/**").hasRole("ADMIN")
        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    // Thêm một lớp Filter kiểm tra jwt
    http.addFilterBefore(jwtAuthenticationFilterAdmin(), UsernamePasswordAuthenticationFilter.class);
  }

}

