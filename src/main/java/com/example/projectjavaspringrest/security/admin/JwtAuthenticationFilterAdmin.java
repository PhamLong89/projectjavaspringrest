package com.example.projectjavaspringrest.security.admin;

import com.example.projectjavaspringrest.service.AdminService;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@Slf4j
public class JwtAuthenticationFilterAdmin extends OncePerRequestFilter {

  @Autowired
  private JwtTokenAdmin tokenAdmin;
  @Autowired
  private AdminService adminService;
  private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationFilterAdmin.class);

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    try {
      // Lấy jwt từ request
      String jwt = getJwtFromRequest(request);
      if (StringUtils.hasText(jwt) && tokenAdmin.validateToken(jwt)) {
        // Lấy id user từ chuỗi jwt
        String email = tokenAdmin.getEmailAdminFromJWT(jwt);
        // Lấy thông tin người dùng từ email
        var adminDetails = (AdminDetails) adminService.loadUserByUsername(
            email);
        if (adminDetails != null) {
          // Nếu người dùng hợp lệ, set thông tin cho Seturity Context
          var authentication = new UsernamePasswordAuthenticationToken(
              adminDetails, null, adminDetails.getAuthorities());
          authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

          SecurityContextHolder.getContext().setAuthentication(authentication);
        }
      }
    } catch (Exception ex) {
      LOGGER.error("Cannot set user authentication: {}", ex.getMessage());
    }
    filterChain.doFilter(request, response);
  }

  private String getJwtFromRequest(HttpServletRequest request) {
    String bearerToken = request.getHeader("Authorization");
    // Kiểm tra xem header Authorization có chứa thông tin jwt không
    if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
      return bearerToken.substring(7);
    }
    return null;
  }
}
