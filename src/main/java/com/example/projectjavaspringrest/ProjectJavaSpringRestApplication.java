package com.example.projectjavaspringrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectJavaSpringRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectJavaSpringRestApplication.class, args);
	}

}
