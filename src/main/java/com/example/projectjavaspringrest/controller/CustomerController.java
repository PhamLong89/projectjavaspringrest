package com.example.projectjavaspringrest.controller;

import com.example.projectjavaspringrest.model.entity.Customer;
import com.example.projectjavaspringrest.model.form.CustomerForm;
import com.example.projectjavaspringrest.security.customer.CustomerDetails;
import com.example.projectjavaspringrest.security.customer.JwtTokenCustomer;
import com.example.projectjavaspringrest.service.CustomerService;
import com.example.projectjavaspringrest.validator.CustomerValidator;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CustomerController {

  private final CustomerValidator customerValidator;
  private final CustomerService customerService;
  @Autowired
  @Qualifier("customer")
  private AuthenticationManager authenticationManager;
  private final JwtTokenCustomer tokenProvider;

  @PostMapping(value = {"/register"})
  public ResponseEntity<List<FieldError>> register(
      @Validated @RequestBody CustomerForm customerForm, BindingResult result) {
    customerValidator.validate(customerForm, result);
    if (!result.hasErrors()) {
      var passwordEncoder = new BCryptPasswordEncoder();
      var customer = new Customer();
      customer.setCustomerName(customerForm.getCustomerName());
      customer.setNumberPhone(customerForm.getNumberPhone());
      customer.setAddress(customerForm.getAddress());
      customer.setEmail(customerForm.getEmail());
      customer.setPassword(passwordEncoder.encode(customerForm.getPassword()));
      customer.setStatusActive(1);
      customerService.save(customer);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
  }

  @PostMapping("/login")
  public ResponseEntity<String> authenticateUser(
      @Validated @RequestBody CustomerForm loginRequest) {
    // Xác thực từ username và password.
    var authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword())
    );
    // Nếu không xảy ra exception tức là thông tin hợp lệ
    // Set thông tin authentication vào Security Context
    SecurityContextHolder.getContext().setAuthentication(authentication);
    // Trả về jwt cho người dùng.
    String jwt = tokenProvider.generateToken((CustomerDetails) authentication.getPrincipal());
    return new ResponseEntity<>(jwt, HttpStatus.OK);
  }

  @GetMapping(value = "/user/profile")
  public ResponseEntity<Customer> getProfile(
      @AuthenticationPrincipal CustomerDetails customerLogged) {
    if (customerLogged == null) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    String email = customerLogged.getUsername();
    var customer = customerService.findByEmail(email);
    if (customer == null) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    return new ResponseEntity<>(customer, HttpStatus.OK);
  }

  @PutMapping(value = "/user/profile")
  public ResponseEntity<List<FieldError>> editProfile(
      @AuthenticationPrincipal CustomerDetails customerLogged,
      @Validated @RequestBody CustomerForm customerForm, BindingResult result) {
    String email = customerLogged.getUsername();
    var customer = customerService.findByEmail(email);
    if (customer == null) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    customerValidator.validateUpdate(customerForm, result);
    if (!result.hasErrors()) {
      customer.setCustomerName(customerForm.getCustomerName());
      customer.setNumberPhone(customerForm.getNumberPhone());
      customer.setAddress(customerForm.getAddress());
      customerService.save(customer);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
  }

  @PutMapping(value = "/user/profile/password")
  public ResponseEntity<List<FieldError>> changePassword(
      @AuthenticationPrincipal CustomerDetails customerLogged,
      @Validated @RequestBody CustomerForm customerForm, BindingResult result) {
    String email = customerLogged.getUsername();
    var customer = customerService.findByEmail(email);
    if (customer == null) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    customerValidator.validateChangePassword(customerForm, customer, result);
    if (!result.hasErrors()) {
      var passwordEncoder = new BCryptPasswordEncoder();
      customer.setPassword(passwordEncoder.encode(customerForm.getNewPassword()));
      customerService.save(customer);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
  }


  @GetMapping(value = {"/manager/customers"})
  public Page<Customer> getCustomersToAdmin(
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage) {
    currentPage = (currentPage < 1) ? 1 : currentPage;
    return customerService.findAll(PageRequest.of(currentPage - 1, 3));
  }

  @GetMapping(value = {"/manager/customer/{customerId}"})
  public ResponseEntity<Customer> getCustomerDetailToAdmin(
      @PathVariable(name = "customerId") int customerId) throws NumberFormatException {
    var customer = customerService.finById(customerId);
    if (customer == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(customer, HttpStatus.OK);
  }

  @GetMapping(value = {"/manager/customers/search"})
  public ResponseEntity<Page<Customer>> searchCustomersByAdmin(
      @RequestParam(name = "key", required = false, defaultValue = "") String customerName,
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage) {
    currentPage = (currentPage < 1) ? 1 : currentPage;
    Page<Customer> customers = customerService.findByName(customerName,
        PageRequest.of(currentPage - 1, 3));
    return new ResponseEntity<>(customers, HttpStatus.OK);
  }

  @PutMapping(value = {
      "/manager/customer/{customerId}"}, produces = "application/json;charset=UTF-8")
  public ResponseEntity<String> changeStatusAccountByAdmin(
      @RequestBody @Validated CustomerForm customerForm, BindingResult result,
      @PathVariable(name = "customerId") int customerId) throws NumberFormatException {
    var customer = customerService.finById(customerId);
    if (customer == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    customerValidator.validateStatus(customerForm, result);
    if (!result.hasErrors()) {
      customer.setStatusActive(Integer.parseInt(customerForm.getStatusActive()));
      customerService.changeStatus(customer);
      return new ResponseEntity<>("Thay đổi trạng thái tài khoản thành công", HttpStatus.OK);
    }
    return new ResponseEntity<>(result.getFieldErrors().get(0).getDefaultMessage(),
        HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({NumberFormatException.class})
  public ResponseEntity<String> ex(NumberFormatException e) {
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }
}
