package com.example.projectjavaspringrest.controller;

import com.example.projectjavaspringrest.model.entity.CartItem;
import com.example.projectjavaspringrest.model.form.CartItemForm;
import com.example.projectjavaspringrest.security.customer.CustomerDetails;
import com.example.projectjavaspringrest.service.CartItemService;
import com.example.projectjavaspringrest.service.CartService;
import com.example.projectjavaspringrest.service.VariantService;
import com.example.projectjavaspringrest.validator.CartItemValidator;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CartItemController {

  private final CartService cartService;
  private final CartItemService cartItemService;
  private final CartItemValidator cartItemValidator;
  private final VariantService variantService;

  @PostMapping(value = {"/cart/item"})
  public ResponseEntity<List<FieldError>> addItem(
      @AuthenticationPrincipal CustomerDetails customerLogged, HttpServletResponse response,
      @CookieValue(value = "userId", defaultValue = "") String userId,
      @RequestBody @Validated CartItemForm cartItemForm, BindingResult result) {
    cartItemValidator.validate(cartItemForm, result);
    if (result.hasErrors()) {
      return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
    }
    var cart = cartService.addCart(userId, customerLogged, response);
    var variant = variantService.findBySKU(cartItemForm.getSKU());
    var cartItem = new CartItem();
    cartItem.setCart(cart);
    cartItem.setQuantity(Integer.parseInt(cartItemForm.getQuantity()));
    cartItem.setVariant(variant);
    if (cart.getCartItems() == null) {
      cartItemService.save(cartItem);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    for (CartItem item : cart.getCartItems()) {
      //nếu đã có sản phẩm trong giỏ hàng thì tăng số lượng
      if (item.getVariant().getSKU().equals(cartItem.getVariant().getSKU())) {
        item.setQuantity(cartItem.getQuantity() + item.getQuantity());
        cartItemValidator.validateQuantity(cartItemService.convertToCartItemForm(item), result);
        if (result.hasErrors()) {
          return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
        }
        cartItemService.save(item);
        return new ResponseEntity<>(HttpStatus.OK);
      }
    }
    cartItemService.save(cartItem);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping(value = {"/cart/item/{id}"})
  public ResponseEntity<String> deleteItem(@AuthenticationPrincipal CustomerDetails customerLogged,
      @CookieValue(value = "userId", defaultValue = "") String userId,
      @PathVariable(name = "id") int id) throws NumberFormatException {
    if (customerLogged != null) {
      userId = String.valueOf(customerLogged.getCustomerId());
    }
    var cart = cartService.findCartUnpaidByUserId(userId);
    if (cartItemService.deleteByIdAndCart(id, cart) > 0) {
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @PutMapping(value = {"/cart/item/{id}"})
  public ResponseEntity<List<FieldError>> updateItem(@PathVariable(name = "id") int id,
      @RequestBody @Validated CartItemForm itemForm, BindingResult result,
      @CookieValue(value = "userId", defaultValue = "") String userId,
      @AuthenticationPrincipal CustomerDetails customerLogged) throws NumberFormatException {
    if (customerLogged != null) {
      userId = String.valueOf(customerLogged.getCustomerId());
    }
    var cart = cartService.findCartUnpaidByUserId(userId);
    var item = cartItemService.findByCartAndId(cart, id);
    if (item == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    cartItemValidator.validateQuantity(itemForm, result);
    if (result.hasErrors()) {
      return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
    }
    item.setQuantity(Integer.parseInt(itemForm.getQuantity()));
    cartItemService.save(item);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @ExceptionHandler({NumberFormatException.class})
  public ResponseEntity<String> ex(NumberFormatException e) {
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

}
