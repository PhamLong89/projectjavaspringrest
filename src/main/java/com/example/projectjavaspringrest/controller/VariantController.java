package com.example.projectjavaspringrest.controller;

import com.example.projectjavaspringrest.model.entity.Variant;
import com.example.projectjavaspringrest.model.form.VariantForm;
import com.example.projectjavaspringrest.service.ProductService;
import com.example.projectjavaspringrest.service.VariantService;
import com.example.projectjavaspringrest.util.ImageUtil;
import com.example.projectjavaspringrest.validator.VariantValidator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class VariantController {

  private final VariantValidator variantValidator;
  private final VariantService variantService;
  private final ProductService productService;
  private static final String VARIANTS = "variants";

  @PostMapping(value = {"/manager/product/variant"})
  public ResponseEntity<List<FieldError>> addVariant(
      @Validated @RequestBody VariantForm variantForm, BindingResult result,
      HttpSession session) throws NumberFormatException, IOException {
    if (variantForm.getProductId() == null || variantForm.getProductId().equals("")) {
      List<VariantForm> variantForms = (List<VariantForm>) session.getAttribute(VARIANTS);
      variantValidator.validate(variantForm, result, variantForms);
      if (result.hasErrors()) {
        return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
      }
      variantForm.setImage1(ImageUtil.uploadImage(variantForm.getFile1()));
      variantForm.setImage2(ImageUtil.uploadImage(variantForm.getFile2()));
      variantForm.setFile1(null);
      variantForm.setFile2(null);
      if (variantForms == null) {
        variantForms = new ArrayList<>();
      }
      variantForms.add(variantForm);
      session.setAttribute(VARIANTS, variantForms);
      return new ResponseEntity<>(HttpStatus.OK);
    }

    var product = productService.findByProductId(Integer.parseInt(variantForm.getProductId()));
    if (product == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    List<VariantForm> variantForms = new ArrayList<>();
    for (Variant variant : product.getVariants()) {
      variantForms.add(variantService.convertToVariantForm(variant));
    }
    variantValidator.validate(variantForm, result, variantForms);
    if (result.hasErrors()) {
      return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
    }
    var variant = new Variant();
    variant.setProduct(product);
    variant.setColor(variantForm.getColor());
    variant.setSize(variantForm.getSize());
    variant.setSKU(variantForm.getSKU());
    variant.setQuantity(Integer.parseInt(variantForm.getQuantity()));
    variant.setImage1(ImageUtil.uploadImage(variantForm.getFile1()));
    variant.setImage2(ImageUtil.uploadImage(variantForm.getFile2()));
    variantService.save(variant);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping(value = {"/manager/product/variant/{sKU}"})
  public ResponseEntity<String> deleteVariant(@PathVariable(name = "sKU") String sKU,
      HttpSession session) {
    List<VariantForm> variantForms = (List<VariantForm>) session.getAttribute(VARIANTS);
    if (variantForms != null) {
      for (VariantForm variant : variantForms) {
        if (sKU.equals(variant.getSKU())) {
          variantForms.remove(variant);
          return new ResponseEntity<>(HttpStatus.OK);
        }
      }
    }
    if (variantService.deleteBySKU(sKU) < 1) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PutMapping(value = {"/manager/product/variant/{sKU}"})
  public ResponseEntity<List<FieldError>> editVariant(@PathVariable(name = "sKU") String sKU,
      @Validated @RequestBody VariantForm variantForm, BindingResult result,
      HttpSession session) throws IOException, NumberFormatException {
    variantForm.setSKU(sKU);
    variantValidator.validateEdit(variantForm, result);
    if (result.hasErrors()) {
      return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
    }
    String fileName1 = ImageUtil.uploadImage(variantForm.getFile1());
    String fileName2 = ImageUtil.uploadImage(variantForm.getFile2());
    if (fileName1 != null) {
      variantForm.setImage1(fileName1);
    }
    if (fileName2 != null) {
      variantForm.setImage2(fileName2);
    }
    var variant = variantService.findBySKU(sKU);
    if (variant != null) {
      variant.setQuantity(Integer.parseInt(variantForm.getQuantity()));
      variant.setImage1(variantForm.getImage1());
      variant.setImage2(variantForm.getImage2());
      variantService.save(variant);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    if (editVariantInSession(variantForm, session) == 1) {
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  public int editVariantInSession(VariantForm variantForm, HttpSession session) {
    List<VariantForm> variantForms = (List<VariantForm>) session.getAttribute(VARIANTS);
    if (variantForms == null) {
      return 0;
    }
    for (VariantForm vF : variantForms) {
      if (variantForm.getSKU().equals(vF.getSKU())) {
        vF.setQuantity(variantForm.getQuantity());
        vF.setImage1(variantForm.getImage1());
        vF.setImage2(variantForm.getImage2());
        variantForms.set(variantForms.indexOf(vF), vF);
        return 1;
      }
    }
    return 0;
  }

  @GetMapping(value = {"/manager/product/variants"})
  public ResponseEntity<List<VariantForm>> getVariantsByProductIdToAdmin(
      @RequestParam(name = "productId", required = false, defaultValue = "") String productId,
      HttpSession session) throws NumberFormatException {
    if (productId == null || productId.equals("")) {
      List<VariantForm> variantForms = (List<VariantForm>) session.getAttribute(VARIANTS);
      return new ResponseEntity<>(variantForms, HttpStatus.OK);
    }
    var product = productService.findByProductId(Integer.parseInt(productId));
    if (product == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    List<VariantForm> variantForms = new ArrayList<>();
    for (Variant variant : product.getVariants()) {
      variantForms.add(variantService.convertToVariantForm(variant));
    }
    return new ResponseEntity<>(variantForms, HttpStatus.OK);
  }

  @GetMapping(value = {"/product/{productId}/variants"})
  public ResponseEntity<List<Variant>> findVariantsByProductAndColorAndSize(
      @RequestParam(name = "color", required = false) String color,
      @RequestParam(name = "size", required = false) String size,
      @PathVariable(name = "productId") int productId) throws NumberFormatException {
    var product = productService.findByProductId(productId);
    if (product == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    if (size == null || size.isEmpty()) {
      List<Variant> variants = variantService.findByProductIdAndColor(productId, color);
      return new ResponseEntity<>(variants, HttpStatus.OK);
    }
    List<Variant> variants = variantService.findByProductIdAndColorAndSize(productId, color, size);
    return new ResponseEntity<>(variants, HttpStatus.OK);
  }

  @ExceptionHandler({NumberFormatException.class})
  public ResponseEntity<String> ex(NumberFormatException e) {
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

}
