package com.example.projectjavaspringrest.controller;

import com.example.projectjavaspringrest.model.entity.Product;
import com.example.projectjavaspringrest.model.entity.Variant;
import com.example.projectjavaspringrest.model.form.ProductForm;
import com.example.projectjavaspringrest.model.form.VariantForm;
import com.example.projectjavaspringrest.security.admin.AdminDetails;
import com.example.projectjavaspringrest.service.ProductService;
import com.example.projectjavaspringrest.service.VariantService;
import com.example.projectjavaspringrest.validator.ProductValidator;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ProductController {

  private final ProductService productService;
  private final ProductValidator productValidator;
  private final VariantService variantService;
  private static final String VARIANTS = "variants";

  @GetMapping(value = {"/products"})
  public ResponseEntity<Page<Product>> getProductHome(
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage)
      throws NumberFormatException {
    Page<Product> pages = productService.findByDisplayHome(PageRequest.of(currentPage - 1, 3));
    return new ResponseEntity<>(pages, HttpStatus.OK);
  }

  @GetMapping(value = {"/products/{displayType}/{category}"})
  public ResponseEntity<Page<Product>> showProductsByCategoryToCustomers(
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage,
      @RequestParam(name = "minPrice", required = false) Integer minPrice,
      @RequestParam(name = "maxPrice", required = false) Integer maxPrice,
      @PathVariable(name = "displayType") String displayType,
      @PathVariable(name = "category") String category) throws NumberFormatException {
    currentPage = (currentPage < 1) ? 1 : currentPage;
    Page<Product> pages = new PageImpl<>(Collections.emptyList());
    switch (displayType) {
      case "category":
        pages = productService.findByCategoryAndProductPriceBetween(category, minPrice, maxPrice,
            PageRequest.of(currentPage - 1, 3));
        break;
      case "new":
        pages = productService.findNewProductsByCategoryAndProductPriceBetween(category, minPrice,
            maxPrice, PageRequest.of(currentPage - 1, 3));
        break;
      case "sale":
        pages = productService.findDiscountProductByCategoryAndProductPriceBetween(category,
            minPrice, maxPrice, PageRequest.of(currentPage - 1, 3));
        break;
      default:
        break;
    }
    return new ResponseEntity<>(pages, HttpStatus.OK);
  }

  @GetMapping(value = {"/product/{productId}", "/manager/product/{productId}"})
  public ResponseEntity<Product> showProductDetail(
      @PathVariable(name = "productId") int productId) throws NumberFormatException {
    var product = productService.findByProductId(productId);
    if (product == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(product, HttpStatus.OK);
  }

  @GetMapping(value = {"/products/{category}"})
  public ResponseEntity<List<Product>> findSimilarProducts(
      @PathVariable(name = "category") String category) {
    List<Product> products = productService.findByCategoryName(category);
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(value = {"/products/search"})
  public ResponseEntity<Page<Product>> searchProductByCustomer(
      @RequestParam(name = "key", required = false, defaultValue = "") String productName,
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage,
      @RequestParam(name = "minPrice", required = false) Integer minPrice,
      @RequestParam(name = "maxPrice", required = false) Integer maxPrice)
      throws NumberFormatException {
    currentPage = (currentPage < 1) ? 1 : currentPage;
    Page<Product> pages = productService
        .findByName(productName, minPrice, maxPrice, PageRequest.of(currentPage - 1, 3));
    return new ResponseEntity<>(pages, HttpStatus.OK);
  }

  @PostMapping(value = {"/manager/product"})
  public ResponseEntity<List<FieldError>> addProduct(
      @AuthenticationPrincipal AdminDetails adminLogged,
      @Validated @RequestBody ProductForm productForm, BindingResult result, HttpSession session) {
    List<VariantForm> variantForms = (List<VariantForm>) session.getAttribute(VARIANTS);
    productForm.setVariants(variantForms);
    productValidator.validate(productForm, result);
    if (!result.hasErrors()) {
      var product = new Product();
      product.setProductName(productForm.getProductName());
      product.setCategoryName(productForm.getCategoryName());
      product.setProductPrice(Integer.parseInt(productForm.getProductPrice()));
      product.setSaleDate(Date.valueOf(java.time.LocalDate.now()));
      product.setDescription(productForm.getDescription());
      product.setProductSale((productForm.getProductSale().trim().length() == 0) ? 0
          : Integer.parseInt(productForm.getProductSale()));
      product.setDisplayHome(Integer.parseInt(productForm.getDisplayHome()));
      product.setAdminId(adminLogged.getAdminId());
      productService.save(product);

      List<Variant> variants = new ArrayList<>();
      for (VariantForm variantForm : variantForms) {
        var variant = new Variant();
        variant.setProduct(product);
        variant.setColor(variantForm.getColor());
        variant.setSize(variantForm.getSize());
        variant.setSKU(variantForm.getSKU());
        variant.setQuantity(Integer.parseInt(variantForm.getQuantity()));
        variant.setImage1(variantForm.getImage1());
        variant.setImage2(variantForm.getImage2());
        variants.add(variant);
      }
      variantService.saveAll(variants);
      session.removeAttribute(VARIANTS);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
  }

  @GetMapping(value = {"/manager/products"})
  public ResponseEntity<Page<Product>> showProductsToAdmins(
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage,
      @RequestParam(name = "sort", required = false, defaultValue = "") String field,
      @RequestParam(name = "type", required = false, defaultValue = "") String type,
      @RequestParam(name = "minPrice", required = false) Integer minPrice,
      @RequestParam(name = "maxPrice", required = false) Integer maxPrice,
      @RequestParam(name = "category", required = false) String category)
      throws NumberFormatException {
    currentPage = (currentPage < 1) ? 1 : currentPage;
    Page<Product> pages;
    boolean existField = field.equals("productId") || field.equals("productName")
        || field.equals("productPrice") || field.equals("productSale")
        || field.equals("categoryName") || field.equals("saleDate");
    if (existField && type.equals("ASC")) {
      pages = productService.findByCategoryAndProductPriceBetween(category, minPrice, maxPrice,
          PageRequest.of(currentPage - 1, 3, Sort.by(field).ascending()));
    } else if (existField && type.equals("DESC")) {
      pages = productService.findByCategoryAndProductPriceBetween(category, minPrice, maxPrice,
          PageRequest.of(currentPage - 1, 3, Sort.by(field).descending()));
    } else {
      pages = productService.findByCategoryAndProductPriceBetween(category, minPrice, maxPrice,
          PageRequest.of(currentPage - 1, 3));
    }

    return new ResponseEntity<>(pages, HttpStatus.OK);
  }

  @GetMapping(value = {"/manager/products/search"})
  public ResponseEntity<Page<Product>> searchProductByAdmin(
      @RequestParam(name = "key", required = false, defaultValue = "") String productName,
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage,
      @RequestParam(name = "sort", required = false, defaultValue = "") String field,
      @RequestParam(name = "type", required = false, defaultValue = "") String type,
      @RequestParam(name = "minPrice", required = false) Integer minPrice,
      @RequestParam(name = "maxPrice", required = false) Integer maxPrice)
      throws NumberFormatException {
    currentPage = (currentPage < 1) ? 1 : currentPage;
    Page<Product> pages;
    boolean existField = field.equals("productId") || field.equals("productName")
        || field.equals("productPrice") || field.equals("productSale")
        || field.equals("categoryName") || field.equals("saleDate");
    if (existField && type.equals("ASC")) {
      pages = productService.findByName(productName, minPrice, maxPrice,
          PageRequest.of(currentPage - 1, 3, Sort.by(field).ascending()));
    } else if (existField && type.equals("DESC")) {
      pages = productService.findByName(productName, minPrice, maxPrice,
          PageRequest.of(currentPage - 1, 3, Sort.by(field).descending()));
    } else {
      pages = productService.findByName(productName, minPrice, maxPrice,
          PageRequest.of(currentPage - 1, 3));
    }
    return new ResponseEntity<>(pages, HttpStatus.OK);
  }

  @PutMapping(value = "/manager/product/{productId}")
  public ResponseEntity<List<FieldError>> editProduct(
      @PathVariable(name = "productId") int productId,
      @Validated @RequestBody ProductForm productForm, BindingResult result,
      @AuthenticationPrincipal AdminDetails adminLogged) throws NumberFormatException {
    var product = productService.findByProductId(productId);
    if (product == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    productValidator.validateEdit(productForm, result);
    if (!result.hasErrors()) {
      product.setProductName(productForm.getProductName());
      product.setCategoryName(productForm.getCategoryName());
      product.setProductPrice(Integer.parseInt(productForm.getProductPrice()));
      product.setDescription(productForm.getDescription());
      product.setProductSale((productForm.getProductSale().trim().length() == 0) ? 0
          : Integer.parseInt(productForm.getProductSale()));
      product.setDisplayHome(Integer.parseInt(productForm.getDisplayHome()));
      product.setAdminId(adminLogged.getAdminId());
      productService.save(product);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
  }

  @DeleteMapping(value = {"/manager/product/{productId}"})
  public ResponseEntity<String> deleteProduct(@PathVariable(name = "productId") int productId)
      throws NumberFormatException {
    if (productService.deleteByProductId(productId) > 0) {
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler({NumberFormatException.class})
  public ResponseEntity<String> ex(NumberFormatException e) {
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

}
