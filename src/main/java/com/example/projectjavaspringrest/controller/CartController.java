package com.example.projectjavaspringrest.controller;

import com.example.projectjavaspringrest.model.entity.Cart;
import com.example.projectjavaspringrest.security.customer.CustomerDetails;
import com.example.projectjavaspringrest.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CartController {

  private final CartService cartService;

  @GetMapping(value = {"/cart/products/total"})
  public int getTotalProductsInCart(@AuthenticationPrincipal CustomerDetails customerLogged,
      @CookieValue(value = "userId", defaultValue = "") String userId) {
    if (customerLogged != null) {
      userId = String.valueOf(customerLogged.getCustomerId());
    }
    var cart = cartService.findCartUnpaidByUserId(userId);
    return cartService.totalProduct(cart);
  }

  @GetMapping(value = {"/cart"})
  public ResponseEntity<Cart> getCart(@AuthenticationPrincipal CustomerDetails customerLogged,
      @CookieValue(value = "userId", defaultValue = "") String userId, Model model) {
    if (customerLogged != null) {
      userId = String.valueOf(customerLogged.getCustomerId());
    }
    var cart = cartService.findCartUnpaidByUserId(userId);
    return new ResponseEntity<>(cart, HttpStatus.OK);
  }

}
