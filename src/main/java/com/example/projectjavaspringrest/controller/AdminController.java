package com.example.projectjavaspringrest.controller;

import com.example.projectjavaspringrest.model.entity.Admin;
import com.example.projectjavaspringrest.model.form.AdminForm;
import com.example.projectjavaspringrest.security.admin.AdminDetails;
import com.example.projectjavaspringrest.security.admin.JwtTokenAdmin;
import com.example.projectjavaspringrest.service.AdminService;
import com.example.projectjavaspringrest.util.ImageUtil;
import com.example.projectjavaspringrest.validator.AdminValidator;
import java.io.IOException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/manager")
@RequiredArgsConstructor
public class AdminController {

  private final AdminValidator adminValidator;
  private final AdminService adminService;
  @Autowired
  @Qualifier("admin")
  private AuthenticationManager authenticationManager;
  private final JwtTokenAdmin tokenProvider;

  @PostMapping(value = {"/admin"})
  public ResponseEntity<List<FieldError>> addAdmin(
      @ModelAttribute("admin") @Validated AdminForm adminForm,
      BindingResult result) throws IOException {
    adminValidator.validate(adminForm, result);
    if (!result.hasErrors()) {
      var passwordEncoder = new BCryptPasswordEncoder();
      var admin = new Admin();
      admin.setAdminName(adminForm.getAdminName());
      admin.setNumberPhone(adminForm.getNumberPhone());
      admin.setSex(adminForm.getSex());
      admin.setAddress(adminForm.getAddress());
      admin.setEmail(adminForm.getEmail());
      admin.setPassword(passwordEncoder.encode(adminForm.getPassword()));
      admin.setImage(ImageUtil.uploadImage(adminForm.getFile()));
      adminService.save(admin);
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(value = {"/admins"})
  public Page<Admin> getAdmins(
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage) {
    currentPage = (currentPage < 1) ? 1 : currentPage;
    return adminService.findAll(PageRequest.of(currentPage - 1, 3));
  }

  @GetMapping(value = {"/admin/{adminId}"})
  public ResponseEntity<Admin> getAdminDetail(@PathVariable(name = "adminId") int adminId)
      throws NumberFormatException {
    var admin = adminService.findById(adminId);
    if (admin == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(admin, HttpStatus.OK);
  }

  @GetMapping(value = {"/admins/search"})
  public ResponseEntity<Page<Admin>> searchAdmin(
      @RequestParam(name = "key", required = false, defaultValue = "") String adminName,
      @RequestParam(name = "page", required = false, defaultValue = "1") int currentPage) {
    currentPage = (currentPage < 1) ? 1 : currentPage;
    Page<Admin> admins = adminService.findByName(adminName, PageRequest.of(currentPage - 1, 3));
    return new ResponseEntity<>(admins, HttpStatus.OK);
  }

  @PostMapping("/login")
  public ResponseEntity<String> authenticateUser(@Validated @RequestBody AdminForm loginRequest) {
    // Xác thực từ username và password.
    var authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword())
    );
    // Nếu không xảy ra exception tức là thông tin hợp lệ
    // Set thông tin authentication vào Security Context
    SecurityContextHolder.getContext().setAuthentication(authentication);
    // Trả về jwt cho người dùng.
    String jwt = tokenProvider.generateToken((AdminDetails) authentication.getPrincipal());
    return new ResponseEntity<>(jwt, HttpStatus.OK);
  }

  @GetMapping(value = "/admin/profile")
  public ResponseEntity<Admin> getProfile(@AuthenticationPrincipal AdminDetails adminLogged) {
    if (adminLogged == null) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    String email = adminLogged.getUsername();
    var admin = adminService.findByEmail(email);
    if (admin == null) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    return new ResponseEntity<>(admin, HttpStatus.OK);
  }

  @PutMapping(value = "/admin/profile")
  public ResponseEntity<List<FieldError>> editProfile(
      @AuthenticationPrincipal AdminDetails adminLogged,
      @Validated @RequestBody AdminForm adminForm, BindingResult result) throws IOException {
    adminValidator.validateAdminUpdate(adminForm, result);
    String email = adminLogged.getUsername();
    var admin = adminService.findByEmail(email);
    if (admin == null) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    if (!result.hasErrors()) {
      admin.setAdminName(adminForm.getAdminName());
      admin.setNumberPhone(adminForm.getNumberPhone());
      admin.setSex(adminForm.getSex());
      admin.setAddress(adminForm.getAddress());
      String fileName = ImageUtil.uploadImage(adminForm.getFile());
      if (fileName != null) {
        admin.setImage(fileName);
      }
      adminService.save(admin);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
  }

  @PutMapping(value = "/admin/profile/password")
  public ResponseEntity<List<FieldError>> changePassword(
      @AuthenticationPrincipal AdminDetails adminLogged,
      @Validated @RequestBody AdminForm adminForm, BindingResult result) {
    String email = adminLogged.getUsername();
    var admin = adminService.findByEmail(email);
    if (admin == null) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    adminValidator.validateChangePassword(adminForm, admin, result);
    if (!result.hasErrors()) {
      var passwordEncoder = new BCryptPasswordEncoder();
      admin.setPassword(passwordEncoder.encode(adminForm.getNewPassword()));
      adminService.save(admin);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({NumberFormatException.class})
  public ResponseEntity<String> ex(NumberFormatException e) {
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

}
