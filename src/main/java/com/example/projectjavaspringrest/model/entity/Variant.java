package com.example.projectjavaspringrest.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "Variants")
@Data
public class Variant implements Serializable {

  @ManyToOne
  @JoinColumn(name = "ProductId")
  @JsonBackReference
  private Product product;

  @Id
  @Column(name = "SKU")
  private String sKU;

  @Column(name = "Color")
  private String color;

  @Column(name = "Size")
  private String size;

  @Column(name = "Quantity")
  private int quantity;

  @Column(name = "Image1")
  private String image1;

  @Column(name = "Image2")
  private String image2;

}
