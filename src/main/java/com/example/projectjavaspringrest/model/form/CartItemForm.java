package com.example.projectjavaspringrest.model.form;

import lombok.Data;

@Data
public class CartItemForm {

  private String id;
  private String quantity;
  private CartForm cartForm;
  private String sKU;
}
