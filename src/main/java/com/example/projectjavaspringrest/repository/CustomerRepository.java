package com.example.projectjavaspringrest.repository;

import com.example.projectjavaspringrest.model.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

  Customer findByEmail(String email);

  @Override
  Customer save(Customer customer);

  @Override
  Page<Customer> findAll(Pageable pageable);

  Customer findByCustomerId(@Param("customerId") int customerId);

  @Modifying
  @Transactional
  @Query(value = "UPDATE Users SET StatusActive = :status WHERE UserID = :customerId", nativeQuery = true)
  void changeStatus(@Param("status") String status, @Param("customerId") int customerId);

  Page<Customer> findByName(String customerName, Pageable pageable);
}
