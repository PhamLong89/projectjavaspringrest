package com.example.projectjavaspringrest.repository;

import com.example.projectjavaspringrest.model.entity.Customer;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepositoryImpl {

  @PersistenceContext
  private EntityManager entityManager;

  public Page<Customer> findByName(String customerName, Pageable pageable) {
    String[] keys = customerName.split(" ");
    var criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<Customer> query = criteriaBuilder.createQuery(Customer.class);
    Root<Customer> customer = query.from(Customer.class);
    List<Predicate> predicates = new ArrayList<>();
    for (String key : keys) {
      predicates.add(criteriaBuilder.like(customer.get("customerName"), "%" + key + "%"));
    }
    query.select(customer)
        .where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
    TypedQuery<Customer> typedQuery = entityManager.createQuery(query);
    typedQuery.setFirstResult((int) pageable.getOffset());
    typedQuery.setMaxResults(pageable.getPageSize());
    List<Customer> customers = typedQuery.getResultList();
// Create Count Query
    CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
    Root<Customer> customerCount = countQuery.from(Customer.class);
    countQuery.select(criteriaBuilder.count(customerCount))
        .where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

    // Fetches the count of all Books as per given criteria
    Long count = entityManager.createQuery(countQuery).getSingleResult();
    return new PageImpl<>(customers, pageable, count);
  }
}
