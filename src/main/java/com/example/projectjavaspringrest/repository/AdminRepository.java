package com.example.projectjavaspringrest.repository;

import com.example.projectjavaspringrest.model.entity.Admin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {

  Admin findByEmail(String email);

  @Override
  Page<Admin> findAll(Pageable pageable);

  Admin findByAdminId(int adminId);

  Page<Admin> findByName(String adminName, Pageable pageable);
}
