package com.example.projectjavaspringrest.repository;

import com.example.projectjavaspringrest.model.entity.Admin;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class AdminRepositoryImpl {

  @PersistenceContext
  private EntityManager entityManager;

  public Page<Admin> findByName(String adminName, Pageable pageable) {
    String[] keys = adminName.split(" ");
    var criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<Admin> query = criteriaBuilder.createQuery(Admin.class);
    Root<Admin> admin = query.from(Admin.class);
    List<Predicate> predicates = new ArrayList<>();
    for (String key : keys) {
      predicates.add(criteriaBuilder.like(admin.get("adminName"), "%" + key + "%"));
    }
    query.select(admin)
        .where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
    TypedQuery<Admin> typedQuery = entityManager.createQuery(query);
    typedQuery.setFirstResult((int) pageable.getOffset());
    typedQuery.setMaxResults(pageable.getPageSize());
    List<Admin> admins = typedQuery.getResultList();
// Create Count Query
    CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
    Root<Admin> adminCount = countQuery.from(Admin.class);
    countQuery.select(criteriaBuilder.count(adminCount))
        .where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

    // Fetches the count of all Books as per given criteria
    Long count = entityManager.createQuery(countQuery).getSingleResult();
    return new PageImpl<>(admins, pageable, count);
  }

}
