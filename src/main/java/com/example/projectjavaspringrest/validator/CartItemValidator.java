package com.example.projectjavaspringrest.validator;

import com.example.projectjavaspringrest.model.form.CartItemForm;
import com.example.projectjavaspringrest.service.VariantService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor
public class CartItemValidator implements Validator {

  private final VariantService variantService;
  private static final String QUANTITY = "quantity";

  @Override
  public boolean supports(Class<?> clazz) {
    return CartItemForm.class.equals(clazz);
  }

  @Override
  public void validate(Object target, Errors errors) {
    var cartItem = (CartItemForm) target;
    String errorSKU = checkSKUExist(cartItem.getSKU());
    if (errorSKU != null) {
      errors.rejectValue("sKU", "sKU", errorSKU);
      return;
    }
    String errorQuantity = checkRemainingProductQuantity(cartItem);
    if (errorQuantity != null) {
      errors.rejectValue(QUANTITY, QUANTITY, errorQuantity);
    }
  }

  public void validateQuantity(Object target, Errors errors) {
    var cartItem = (CartItemForm) target;
    String errorQuantity = checkRemainingProductQuantity(cartItem);
    if (errorQuantity != null) {
      errors.rejectValue(QUANTITY, QUANTITY, errorQuantity);
    }
  }

  public String checkSKUExist(String sKU) {
    if (sKU == null || sKU.isEmpty() || variantService.findBySKU(sKU) == null) {
      return "Màu sắc và kích cỡ sản phẩm không tồn tại";
    }
    return null;
  }

  public String checkRemainingProductQuantity(CartItemForm itemForm) {
    var variant = variantService.findBySKU(itemForm.getSKU());
    if (variant == null) {
      return null;
    }
    if (variant.getQuantity() == 0) {
      return "Hết hàng.";
    }
    try {
      if (Integer.parseInt(itemForm.getQuantity()) < 1) {
        return "Số lượng không hợp lệ";
      }
    } catch (NumberFormatException e) {
      return "Số lượng không đúng định dạng";
    }
    if (Integer.parseInt(itemForm.getQuantity()) > variant.getQuantity()) {
      return "Số lượng sản phẩm không thể đáp ứng. Vui lòng chọn lại số lượng ";
    }
    return null;
  }

}
